$(function(){
    //调用加载数据函数
    loadstu();
    
    //为添加按钮绑定一个添加动作
    $("#aid").click(function(){
        $("#fill").show();
        $("#add_form").show();
    });
});

//加载信息
function loadstu(){
    $.ajax({
        url:"./model_action.php?a=index",
        type:"GET",
        dataType:"json",
        success:function(data){
            //alert(data);
            var str=null;
            for(var i=0;i<data.length;i++){
                str+="<tr>";
                if(data[i].status == 1){
                    str+="<td><input type='checkbox' checked /></td>";
                }else{
                    str+="<td><input type='checkbox'  /></td>";
                }
                str+="<td>"+data[i].title+"</td>";
                str+="<td><a href=\"javascript:void(0);\" onclick='doDel("+data[i].id+",this)'>DELETE</a>&nbsp;&nbsp;<a href=\"javascript:void(0);\" onclick='display("+data[i].id+",this)'>UPDATE</a></td>";
                str+="</tr>";
                
            }
            $("#stuid tbody").empty();
            $("#stuid tbody").append(str);
        }
    });
}

//删除函数
function doDel(sid,ob){
    //执行ajax删除
    if(confirm("Are you sure to DELETE?"))
    $.ajax({
        url:"./model_action.php?a=del",
        type:"POST",
        data:{id:sid},
        dataType:"json",
        success:function(res){
            //判断是否删除成功
            if(res.num>0){
                //alert(res.num);
                $(ob).parents("tr").remove();
            }
        },
        error:function(){
            alert('error');
        }
    });
}

//执行添加
function doAdd(form){
    //alert($(form).serialize());
    $.ajax({
        url:"./model_action.php?a=insert",
        type:"POST",
        data:$(form).serialize(),
        dataType:"json",
        success:function(res){
            if(res.id>0){
                $('#fill').hide();
                $('#add_form').hide();
                $(form).find(":reset").trigger("click");
                loadstu();//重新加载
            }else{
                alert('ADD Fails!');
            }
        }
    });
    
    return false;
}

//display
function display(sid,ob){
    $("#fill").show();
    $("#update_form_div").show();
    $.ajax({
        url:"./model_action.php?a=display",
        type:"POST",
        data:{id:sid},
        dataType:"json",
        success:function(res){
            if(res.status==1){
                $("#update_status").attr("checked", 'checked');
            }else{
                $("#update_status").attr("checked", false);
            }
            $("#update_title").val(res.title);
            $("#update_id").val(res.id);
        },
        error:function(){
            alert('error');
        }
    });
}

//do update
function doUpdate(form){
    //alert($(form).serialize());
    $.ajax({
        url:"./model_action.php?a=doUpdate",
        type:"POST",
        data:$(form).serialize(),
        dataType:"json",
        success:function(res){
            if(res.num>0){
                $('#fill').hide();
                $('#update_form_div').hide();
                $(form).find(":reset").trigger("click");
                loadstu();//重新加载
            }else{
                alert('update Fails!');
            }
        }
    });
    
    return false;
}