<?php
header("Content-type: text/html; charset=utf-8");
include('./db.php');

$action = $_GET['a'];

if($action == 'index'){
    
    $sql = "SELECT * FROM `to_do_list`";
    $items=array();
    foreach ($dbh->query($sql) as $row) {
        //var_dump($row);
        array_push($items, $row);
        
    }
    echo(json_encode($items));
}

if($action == 'insert'){
    //var_dump($_POST);
    $title = $_POST['title'];
    if(isset($_POST['status'])){
        $status = 1;
    }else{
        $status = 0;
    }
    $sql = "INSERT INTO `to_do_list`(`title`, `status`) VALUES ('".$title."',".$status.")";
    $rs = array();
    if ($dbh->query($sql) == TRUE) {
        $rs['id'] = $dbh->lastInsertId();
        echo(json_encode($rs));
    } else {
        echo "Error: " . $sql . "<br>" . $dbh->error;
    }
}

if($action == 'del'){
    $id = $_POST['id'];
    $sql = "DELETE FROM `to_do_list` WHERE `id`=".$id;
    $rs = array();
    $rs['num'] = $dbh->exec($sql);
    //echo($rs['num']);
    if($rs['num']) {
        echo(json_encode($rs));
    }else{
        echo "Error: " . $sql . "<br>" . $dbh->error;
    }
}

if($action == 'display'){
    $id = $_POST['id'];
    $sql = "SELECT * FROM `to_do_list` WHERE `id`=".$id;
    $items=array();
    foreach ($dbh->query($sql) as $row) {
        //var_dump($row);
        array_push($items, $row);
    }
    //var_dump($row);
    echo(json_encode($row));
}

if($action == 'doUpdate'){
    $id = $_POST['update_id'];
    $title = $_POST['update_title'];
    if(isset($_POST['update_status'])){
        $status = 1;
    }else{
        $status = 0;
    }
    $sql = "UPDATE `to_do_list` SET `title`='".$title."',`status`='.$status.' WHERE `id`=".$id;
    $rs = array();
    $rs['num'] = $dbh->exec($sql);
    if($rs['num']) {
        echo(json_encode($rs));
    }else{
        echo "Error: " . $sql . "<br>" . $dbh->error;
    }
}


?>