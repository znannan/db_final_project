CREATE TABLE `db_final_project`.`to_do_list` ( 
	`id` INT(4) NOT NULL AUTO_INCREMENT , 
	`title` VARCHAR(255) NOT NULL , 
	`states` BOOLEAN NOT NULL , 
	PRIMARY KEY (`id`)
) ENGINE = InnoDB;